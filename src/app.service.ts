import { Injectable, HttpService } from '@nestjs/common';


@Injectable()
export class AppService {
  constructor(private httpService: HttpService) {}
  async getHello() {
    return await this.getTokenAsync();
  }

  async getTokenAsync() {
    var url = 'https://login.microsoftonline.com/xxxxxx-xxxxxx-xxx-xxx-xxxxxx/oauth2/v2.0/token';
    const scope = 'api://[YOUR API CLEINT ID]/.default';
    const clientSecret = '[YOUR CLIENT, CLIENT SECRET]';
    const grantType = 'client_credentials';
    const clientId = '[YOUR CLIENT, CLIENTID]';
    const data = `client_id=${clientId}&client_secret=${clientSecret}&grant_type=${grantType}&scope=${scope}`;
    console.log('Request token');
    const response = await this.httpService.post(url, data).toPromise();
    console.log(response.data);
    return response.data.access_token;
  }
}
